FROM centos:7

USER root


RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

RUN yum -y update


#INSTALL APACHE-HTTPD
RUN yum install httpd -y
RUN yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm -y
RUN yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
RUN yum install yum-utils make which -y
RUN yum-config-manager --enable remi-php72
RUN yum install php php-fpm php-gd php-json php-mbstring php-pdo php-mysqlnd php-xml php-xmlrpc php-opcache php-pear php-devel -y

#CREATE PHP-FPM RUNNER
RUN mkdir /run/php-fpm 

#INSTALL NETWORK TOOLS
RUN yum install net-tools -y
RUN yum install iputils -y

#DATABASES DRIVER SQLSRV
RUN curl https://packages.microsoft.com/config/rhel/7/prod.repo > /etc/yum.repos.d/mssql-release.repo
RUN yum remove unixODBC-utf16 unixODBC-utf16-devel
RUN ACCEPT_EULA=Y yum install msodbcsql -y
RUN ACCEPT_EULA=Y yum install mssql-tools -y
RUN yum install unixODBC-devel make -y
RUN yum install php72-php-sqlsrv.x86_64 php-sqlsrv php-pdo_sqlsrv -y 

#LOAD DATABASE MODULES
RUN echo extension=pdo_sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/30-pdo_sqlsrv.ini
RUN echo extension=sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/20-sqlsrv.ini
RUN rm  /etc/php.d/40-sqlsrv.ini

#INSTALL COMPOSER
RUN php -r "copy('http://getcomposer.org/installer', 'composer-setup.php');" 
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer


#INSTALL SUPERVISOR FOR RUNNING HTTPD AND PHP FPM
RUN yum -y install supervisor
RUN mkdir -p /run/supervisor
RUN chmod 777 /run/supervisor/ -R
 
#CONFIGURE APACHE
#GIVE PERMISSION FOR user-group-other 766 -> for write log & session
RUN chmod 766 -R /var/www/html && \
    sed -i 's/Listen 80/Listen 20001/' /etc/httpd/conf/httpd.conf


#SET DEFAULT WORKING DIR
WORKDIR /var/www/html

#CREATE PHPINFO FILE
RUN echo "<?php phpinfo(); ?>" > /var/www/html/index.php

EXPOSE 8888

CMD ["/usr/bin/supervisord"]
